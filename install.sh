# Default keyboard layout is US.
# To change layout :
# 1. Use `localectl list-keymaps` to display liste of keymaps.
# 2. Use  `loadkeys [keymap]` to set keyboard layout.
#    ex : `loadkeys de-latin1` to set a german keyboard layout.

# Check if system is booted in UEFI mode.
if [ ! -d "/sys/firmware/efi/efivars" ] 
then
    # If not, then exit installer.
    echo "[Error!] System is not booted in UEFI mode. Please boot in UEFI mode & try again."
    exit 9999
fi

# Figure out how much RAM the system has an set a variable
ramTotal=$(free -h | awk '/^Mem:/{print $2}'| awk -FG {'print$1'})

# Set append for drive automation
APPEND=""

# Update system clock.
timedatectl set-ntp true

# Load kernel modules
modprobe dm-crypt
modprobe dm-mod

# Detect and list the drives.
lsblk -f

# Choice the drive to use :
# 1. 
echo "----------"
echo ""
echo "Which drive do we want to use for this installation?"
read driveName

(
echo g       # Create new GPT partition table
echo n       # Create new partition (for EFI).
echo         # Set default partition number.
echo         # Set default first sector.
echo +1G     # Set +1G as last sector.
echo n       # Create new partition (for root).
echo         # Set default partition number.
echo         # Set default first sector.
echo         # Set last sector.
echo t       # Change partition type.
echo 1       # Pick first partition.
echo 1       # Change first partition to EFI system.
echo w       # write changes. 
) | sudo fdisk $driveName -w always -W always

# List the new partitions.
lsblk -f

if [[ "$driveName" == "/dev/nvme"* || "$driveName" == "/dev/mmcblk0"* ]]; then
  APPEND="p"
fi

efiName=${driveName}$APPEND
efiName+=1
rootName=${driveName}$APPEND
rootName+=2
swapName=${driveName}$APPEND
swapName+=3

echo ""
echo "Do you want Hibernation?"
echo "1) Yes"
echo "2) No"
read hibState

# Create EFI partition
sudo mkfs.fat -F32 -n EFI $efiName     

# Encrypt the root partition
sudo cryptsetup luksFormat -v -s 512 -h sha512 $rootName

# Open the encrypted root partition
sudo cryptsetup luksOpen $rootName crypt-root

sudo pvcreate /dev/mapper/crypt-root
sudo vgcreate lvm /dev/mapper/crypt-root

if [ $hibState = 1 ]; then
   sudo lvcreate -L "$ramTotal"G -n swap lvm

elif [ $hibState = 2 ]; then
   sudo lvcreate -L 4G -n swap lvm

fi

sudo lvcreate -l '100%FREE' -n root lvm

sudo cryptsetup config $rootName --label luks

sudo mkswap /dev/lvm/swap              # swap partition
sudo mkfs.btrfs -L root /dev/mapper/lvm-root  # /root partition

# Mount the filesystems.
sudo swapon /dev/mapper/lvm-swap
sudo mount /dev/mapper/lvm-root /mnt

# Create Subvolumes
btrfs subvolume create /mnt/@root
btrfs subvolume create /mnt/@home

# Unmount root
sudo umount /mnt

# Mount the subvolumes.
mount -o noatime,commit=120,compress=zstd:10,subvol=@root /dev/lvm/root /mnt
mkdir /mnt/home
mount -o noatime,commit=120,compress=zstd:10,subvol=@home /dev/lvm/root /mnt/home

# Mount the EFI partition.
mount --mkdir $efiName /mnt/boot/

# Enable Pacman Parallel Downloads
sed -i 's/#ParallelDownloads/ParallelDownloads/g' /etc/pacman.conf

# Install essential packages.
pacstrap /mnt base base-devel linux linux-headers linux-lts linux-lts-headers linux-firmware btrfs-progs nano man lvm2 dhcpcd

# Generate fstab file.
genfstab -U /mnt >> /mnt/etc/fstab

# Fetch script for `arch-chroot`.
curl https://gitlab.com/ahoneybun/arch-itect/-/raw/main/setup.sh > /mnt/setup.sh

# Change root into the new system & run script.
arch-chroot /mnt sh setup.sh "$rootName"

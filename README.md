# The Arch-itect Installer

This installer does the following at it's core:

- Partition the drive of your choice
- Encrypts the installation
- Installs an Arch base + base-devel
- Installs DE/WM of my choice
- Installs other tools that I use
- Installs my Plasma config files (if picked during installation) from another repo on Gitlab

Tested on the following drives:
- SATA 
- M.2 NVMe

## Get Started

Prerequisites:

- Prepare an installation medium.
- Boot the live environment.
- Connect to internet.

## Connect to internet

- `iwctl`
- `station device connect SSID`
- `exit`

## Start the installer

```
curl https://gitlab.com/ahoneybun/arch-itect/-/raw/main/install.sh > install.sh; sh install.sh
```

or 

```
sh <(curl -L https://gitlab.com/ahoneybun/arch-itect/-/raw/main/install.sh)
```

The following will happen:

- Clear partition table for `/dev/***`.
- Creates a GPT partition table for `/dev/***`.
- Create a +512M EFI partiton at `/dev/***1`.
- Create a root partition with BTRFS at `/dev/***2` with @root and @home subvolumes.
- Create a swap partition at `/dev/***3` with the choice to set it as the same size as the RAM for hibernation.
- Install base, base-devel, linux[-lts] linux-headers[-lts], linux-firmware, nano, lvm2.
- Set localization, hostname and create regular user.
- Install systemd-boot.
- Install other tools that I need.

Note you will be prompted to specify the `hostname`, `username` and `passwd`.

## After Installation

Setup virt-manager:

https://forum.manjaro.org/t/can-not-get-kvm-to-show-in-virt-manager/27841/4

## Roadmap

- [ ] 
- [ ]

# Possible Ideas

- [ ] Allow separate home partition (useradd -mdG $userName) + (create different partition scheme)
